package view;
import model.FunctionStorage;

import java.util.Scanner;

public class main {
    public static void main(String[] args){
        String opt;
        Scanner sc = new Scanner(System.in);
        FunctionStorage fs = new FunctionStorage();
        fs.printGroupName();
        while(true){
            fs.printMenu();
            System.out.print("Command --> ");
            opt=sc.next();
            switch (opt){
                case "*":
                    fs.display();
                    break;
                case "w":
                    System.out.println("w");
                    break;
                case "r":
                    System.out.println("r");
                    break;
                case "u":
                    System.out.println("u");
                    break;
                case "d":
                    System.out.println("d");
                    break;
                case "f":
                    System.out.println("f");
                    break;
                case "p":
                    System.out.println("p");
                    break;
                case "n":
                    System.out.println("n");
                    break;
                case "l":
                    System.out.println("l");
                    break;
                case "s":
                    System.out.println("s");
                    break;
                case "g":
                    System.out.println("g");
                    break;
                case "se":
                    System.out.println("se");
                    break;
                case "sa":
                    System.out.println("sa");
                    break;
                case "ba":
                    System.out.println("ba");
                    break;
                case "re":
                    System.out.println("re");
                    break;
                case "h":
                    System.out.println("h");
                    break;
                case "e":
                    System.exit(0);
                default:
                    System.out.println("Invalid Input");
                    break;
            }
        }

    }
}
