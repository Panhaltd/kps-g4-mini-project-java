package util;

public class StockItem {
    private int id;
    private String Name;
    private Double unitPrice;
    private int qty;
    private String importedDate;

    public StockItem(int id, String name, Double unitPrice, int qty, String importedDate) {
        this.id = id;
        Name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.importedDate = importedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }
}
