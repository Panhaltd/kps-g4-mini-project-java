package model;

import com.jakewharton.fliptables.FlipTable;
import constant.ConstStr;
import java.sql.*;

public class FunctionStorage extends Thread{
    public void printGroupName(){
        System.out.println("\t\t\t\t\t\t\t\t\t\tWelcome To");
        System.out.println("\t\t\t\t\t\t\t\t\tStock Management");
        try {
            String[] str = ConstStr.groupName.split("\n");
            for (String s : str) {
                System.out.println(s);
                sleep(300);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void printMenu(){
        String[] menu1 = {"*)Display","W)rite","R)ead","U)pdate","D)elete","F)irst","P)revios","N)ext","L)ast"};
        String[][] menu2 = {
                { "S)earch","G)oto","Se)t row","Sa)ve","Ba)ck up","Re)store","H)elp","E)xit"," "},
        };
        System.out.println(FlipTable.of(menu1,menu2));
    }

    public void display(){
        Connection con = null;
        Statement presmt = null;
        String[] header = {"ID","Name","Unit Price","Quantity","Imported"};

        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/kps-g4"
                            ,"postgres","Panhalong11");
            presmt= con.createStatement();
            ResultSet rs1 = presmt.executeQuery("SELECT * FROM stockdata");

            int row=0;
            while (rs1.next()){
                row++;
            }

            ResultSet rs = presmt.executeQuery("SELECT * FROM stockdata");
            String[][] item=new String[row][5];
            int i=0;
            while (rs.next()){
                item[i][0]=Integer.toString(rs.getInt("id"));
                item[i][1]=rs.getString("name");
                item[i][2]=Double.toString(rs.getDouble("unitPrice"));
                item[i][3]=Integer.toString(rs.getInt("qty"));
                item[i][4]=rs.getString("importedDate");
                i++;
            }
            System.out.println(FlipTable.of(header,item));

        }catch (Exception e){
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }
}
